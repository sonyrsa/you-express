const promise  = require('bluebird');
const options = {
  promiseLib: promise, // Initialization Options
  capSQL: true // generate capitalized SQL 
};
const pgp = require('pg-promise')(options);
const dbString = process.env.NODE_ENV === 'production' 
? 'postgres://sxuoupiibspmit:1052f35d952f1689333dc0e00c4d5e25c90adaacf797be5a04bb7299beb5b957@ec2-174-129-206-173.compute-1.amazonaws.com:5432/d9tka75ue1l07e?ssl=true'
: 'postgres://postgres:postgres@localhost:5432/express';
const db = pgp(dbString);
//const db = pgp('postgres://sxuoupiibspmit:1052f35d952f1689333dc0e00c4d5e25c90adaacf797be5a04bb7299beb5b957@ec2-174-129-206-173.compute-1.amazonaws.com:5432/d9tka75ue1l07e?ssl=true');

module.exports = db;

